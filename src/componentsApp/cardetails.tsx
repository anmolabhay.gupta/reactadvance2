import React from "react";
import  { useEffect, useState } from 'react';
import logo from "./icon1.jpg";
import "./apps.css";
 
import { makeStyles } from '@material-ui/core/styles';
import {Navbar} from '../common/navbar';
import pic1 from  "../images/MG.jpg"
import pic2 from  "../images/MGex.jpg"
import pic3 from  "../images/renault1.jpg" 
import pic4 from  "../images/renault2.jpg" 

import { Link } from "react-router-dom";

export interface RootObject {
    id: number;
    name: string;
    type:number
    efficiency: number;
    Charging: number;
    Range: number;
    features: string;
    Available_from: string;
    price:number;

}
interface IProps {
    data:RootObject;
    setConfirmed: (car: RootObject) => void;
   }
   
   const CarDetails:React.FC<IProps>= ({ data,setConfirmed}) => {

    
    return(
        
        <div>
      <Navbar/>
      <div className="parent">
      <div className="cardets kid">
          <div className="ad">{data.name}</div>
      <img className="sc" src={data.type === 1 ? pic1: data.type === 2 ? pic2  :data.type===3?pic3: pic4} alt="car-img"></img></div>
     
      <div className="kid">
          Available_from : {data.Available_from}
      </div>
      <div className=" fr kid">
          <div>{data.name}</div>
          <div className="space">Min duration</div>
          <div className="parent space"><button className="kid pink">1 months</button><button className="kid">6 months</button></div>
          <div className="space">Miles per month</div>
          <div className="parent space"><button className="kid pink">800</button><button className="kid">1000</button><button className="kid pink">1200</button></div>
          <div className="space">Delivery Date</div>
          <div>{data.Available_from}</div>
          <div className=" space">
              <Link to="/confirmed"><button onClick={() => setConfirmed(data)}  className="pink" >Book now</button></Link></div>

      </div>
     
     
      </div>
      <div className="lu" >
         <span className="lk"><strong>{data.features}</strong> </span>
      </div>
     

      

        </div>




    ); 



}
export default CarDetails;
   