import React from "react";
import  { useEffect, useState } from 'react';
import logo from "./icon1.jpg";
import "./apps.css";
 
import { makeStyles } from '@material-ui/core/styles';
import {Navbar} from '../common/navbar';
import pic1 from  "../images/MG.jpg"
import pic2 from  "../images/MGex.jpg"
import pic3 from  "../images/renault1.jpg" 
import pic4 from  "../images/renault2.jpg"
import battery from "../images/battery.jpg" ;
import road from "../images/road.jpg" ;

export interface RootObject {
    id: number;
    name: string;
    type:number
    efficiency: number;
    Charging: number;
    Range: number;
    features: string;
    Available_from: string;
    price:number;

}
interface IProps {
    data:RootObject;

   }
   const ConfirmedBooking:React.FC<IProps>= ({ data}) => {
       return(
           <div>
               <Navbar/>
               <div className=" space lefttext">My Booking</div>
               <div>
               <div className="kid1 lefttext ">Date : {data.Available_from}</div>
               <div className="lj"><div className="parent1 space">
                       <div className="kid1 space"><strong>{data.name}</strong></div>
                      </div>
                    <div className="parent1 space">
                        <div className="kid1">  <img className="sc" src={data.type === 1 ? pic1: data.type === 2 ? pic2  :data.type===3?pic3: pic4} alt="car-img"></img></div> 
                        <div className="kid1">
                            <div className="space">
                                <strong>Rs. 570</strong>
                            </div><div >
                                Annual fuel savings
                            </div>
                            <div className="space">
                                <strong>100%</strong>
                            </div>
                            <div>
                                trips covered by home 
                            </div><div>and work change points</div>
                            <div className="space"><strong>0</strong></div>
                            <div>Annual public charging uses</div>
                            <div className="Parent">
                                <div className="kid"><img className=" sizu" src={battery}/><div >40 kWh </div></div>
                                <div className="kid"><img className=" sizu" src={road}/><div >81 miles </div></div>
                            </div>
                            
                                
                                
                            

                            </div>  



                   </div>
</div>
                   


               </div>
           </div>



       );



   }
   export default ConfirmedBooking;
   