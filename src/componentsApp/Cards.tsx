import React from "react";
import logo from "./icon1.jpg";
import pic1 from  "../images/MG.jpg"
import pic2 from  "../images/MGex.jpg"
import pic3 from  "../images/renault1.jpg" 
import pic4 from  "../images/renault2.jpg" 
import { debounce } from 'ts-debounce';

import { makeStyles } from '@material-ui/core/styles';
import CardDetails from './cardetails'
import  { useEffect, useState } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    
    Redirect,
    useParams,
    useRouteMatch
  } from "react-router-dom";

import {
    Grid,
    Card,
    CardContent,
    Typography,
    CardHeader
} from '@material-ui/core/'
import { NavLink } from "react-bootstrap";
import { Link } from "react-router-dom";

export interface RootObject {
    id: number;
    name: string;
    type:number;
    efficiency: number;
    Charging: number;
    Range: number;
    features: string;
    Available_from: string;
    price:number;

}



interface IProps {
   data:RootObject[];
   setcardata1: (car: RootObject) => void;
  }
  
  
 
  export const CardList:React.FC<IProps>= ({ data,setcardata1}) => {

    
    const[search,setSearch]=useState('');
    const[filteredCars,setFilteredCars]=useState<RootObject[]>(data);
    const[sort,setSort]=useState('');
    const[cardata,setcardata]=useState<RootObject[]>(data);
    const debouncedTest = debounce(setSearch, 500);
    useEffect(()=>{
        console.log("search", search);
       
        setFilteredCars(
         cardata.filter((instance) =>
         
         instance.name.toLowerCase().includes(search.toLowerCase())
 
        ))
 
        
       
       },
       
       [search,cardata])

    var comparePrice=(car1:RootObject,car2:RootObject)=>{
        if(car1.price>car2.price){
            return -1;
        }
        if(car1.price<car2.price){
            return 1;
        }
        return 0;
        

    }
    var comparePricedes=(car1:RootObject,car2:RootObject)=>{
        if(car1.price>car2.price){
            return 1;
        }
        if(car1.price<car2.price){
            return -1;
        }
        return 0;
        

    }
    const Sorting=( s:String)=>{

        if(s==="asc"){
            setFilteredCars(filteredCars.sort(comparePrice));
            setSort("asc");
         
        }
        else{

            setFilteredCars(filteredCars.sort(comparePricedes));
            setSort("des");
            

        }
        
    }


      console.log("data232323",cardata);
      
      
      return( 
      
      <div >
           <div className="parent">
                <div className="kid"><form>  <label >Sort by</label>
        <select name="cars" id="cars" onChange={(e)=>Sorting(e.target.value)}>
         <option value="des">low to high</option>
         <option value="asc">high to low</option>  </select></form></div> 
           
    
      
             <div className="kid"><input type="text" placeholder="search.." onChange={(e)=>debouncedTest(e.target.value)}/></div></div>
        <Grid
            container
            spacing={2}
            direction="row"
            justify="flex-start"
            alignItems="flex-start"
        >
        
        {filteredCars.map((car)=> (
             
             <Grid item xs={5} sm={5} md={4} key={cardata.indexOf(car)}><Card>
             <div className="io">Price : {car.price}</div>
             <CardContent>
                            <Typography variant="h5" gutterBottom>
                               
                            <img src={car.type === 1 ? pic1: car.type === 2 ? pic2  :car.type===3?pic3: pic4} alt="car-img"></img>
                           {console.log("carr type",car.type)} 
                              <div>Name-{car.name}</div>  
                                <div>Range -{car.Range}</div>

                            <Link to="/cardetails"><button onClick={() => setcardata1(car)}>Explore</button> </Link>     

                            </Typography>
                        </CardContent>           


             </Card></Grid>
                 
               
           
                  
                ))}
          



     </Grid>
      </div>);
  }