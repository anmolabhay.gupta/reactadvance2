import react from 'react';
import './comm.css';
import logo from "./icon1.jpg";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useParams,
  useRouteMatch
} from "react-router-dom";
import { propTypes } from 'react-bootstrap/esm/Image';


 export function Navbar(){
     return (
        <div className="navbar">
          
          <img src={logo} alt="text" className="logo"></img>
        
          
        
 
   <div className="topnav-right ">
     <Link to="/"><span className="cour">Electric Cars</span></Link>
    <Link to="/sustaintool"><span className="cour">Suitability Tools</span></Link> 
     <Link to="/confirmed"><span className="cour">My bookings</span></Link>
     <Link to="signedout"><span className="right_icons">Sign out</span></Link> </div></div>
  


     );
 }
 const mapStateToProps = (state: any) => {
  return { ...state };
};