import React from "react";
import "./App.css";
import { connect } from "react-redux";
import LoginForm from "./components/login/container/LoginForm";
import {Electriccars} from "./componentsApp/electricCars";
import {Home} from "./home";
function App(props: any) {
  return (
    <div className="App">
      {!props.authStatus? (
        <LoginForm {...props} />
      ) : 
      (<Home/>)
        
      }
    </div>
  );
}

const mapStateToProps = (state: any) => {
  return { ...state };
};

export default connect(mapStateToProps, null)(App);
