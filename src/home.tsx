import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import {createStore} from 'redux';
import allReducers   from './Reducers/allReducers';
import {Electriccars} from "./componentsApp/electricCars";
import CarDetails from "./componentsApp/cardetails";
import ConfirmedBooking from "./componentsApp/confirmed";
import Sustain from "./componentsApp/sustainabilitytools";
import  { useEffect, useState } from 'react';
import { compose } from 'redux';
import LoginForm from "./components/login/container/LoginForm";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    Redirect,
    useParams,
    useRouteMatch
  } from "react-router-dom";
  export interface RootObject {
    id: number;
    name: string;
    type:number
    efficiency: number;
    Charging: number;
    Range: number;
    features: string;
    Available_from: string;
    price:number;
}
let myObj:RootObject = { id:0, name: "",efficiency:0,type:1,Charging:0,Range:0,features:"",Available_from:"",price:0 };

export function Home() {
   
    const store1=createStore(allReducers);
    const[cardata1,setcardata1]=useState<RootObject>(myObj);
    const[confirmed,setConfirmed]=useState<RootObject>(myObj);
   
      return (
        <Provider store={store1}>
        <Router>
        <Switch>
       
        <Route exact path="/"  render={(props)=><Electriccars setcardata1={setcardata1}/>} />
        <Route  path="/cardetails" render={(props)=><CarDetails data={cardata1} setConfirmed={setConfirmed}/>}/>
        <Route  path="/confirmed" render={(props)=><ConfirmedBooking data={cardata1} />}/>
        <Route path="/sustaintool" component={Sustain}/>

        <Route path="/signout" component={LoginForm}/>
         
       
      
        </Switch>
        </Router>
        </Provider>
          
    
        
    
    
      );
      
    
    
}


