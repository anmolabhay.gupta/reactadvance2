import firebase from "firebase/app";
import "firebase/auth";

const firebaseApp = firebase.initializeApp({
  // apiKey: process.env.REACT_APP_FIREBASE_KEY,
  // authDomain: process.env.REACT_APP_FIREBASE_DOMAIN,
  // projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
  // storageBucket: process.env.REACT_APP_FIRESEBASE_STORAGE_BUCKET,
  // messagingSenderId: process.env.REACT_APP_FIREBASE_SENDER_ID,

  apiKey: "AIzaSyDfh0jsTh5_eNLLS96Tznt1feuiB_NwBAA",
  authDomain: "newproj-d59b7.firebaseapp.com",
  projectId: "newproj-d59b7",
  storageBucket: "newproj-d59b7.appspot.com",
  messagingSenderId: "354435518347",
  appId: "1:354435518347:web:0dab0e5933f5505310e98e",
  measurementId: "G-K7E4X42CKS"
});

export default firebaseApp;
